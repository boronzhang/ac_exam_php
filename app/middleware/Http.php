<?php

declare(strict_types=1);

namespace app\middleware;

use app\common\model\account\User as UserModel;
use app\control\model\User;
use app\Request;
use Closure;
use think\Session;

/**
 * Class Http
 * @package app\middleware
 */
class Http
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $moduleName = $request->baseUrl();
        $moduleName = explode('/', $moduleName);
        if (!empty($moduleName[1])) {
            switch ($moduleName[1]) {
                case 'control':
                    $id = session('exam_control');
//                    $id = 1;
                    if (empty($id)) {
                        abort(401, '未登录');
                    }
                    $userInfo = UserModel::fetch(intval($id));
                    if (empty($userInfo)) {
                        abort(401, '账号未授权');
                    }
                    $currentUser = $userInfo;
                    $isRole = self::role($currentUser, $moduleName);
                    if (!$isRole) {
                        die;
                    }
                    session('currentUser', serialize($currentUser));
                    break;
                case 'dock':
                    if (isset($moduleName[2]) && isset($moduleName[3]) && ($moduleName[2] == 'exam') && ($moduleName[3] == 'fetch')) {
                        break;
                    }
                    $id = session('exam_dock');
                    if (empty($id)) {
                        abort(401, '未登录');
                    }
                    $userInfo = UserModel::fetch(intval($id));
                    if (empty($userInfo)) {
                        abort(401, '账号未授权');
                    }
                    session('currentUser', serialize($userInfo));
                    break;
                case 'wander':
                    break;
                default:
//                    abort(401, '错误路径');
                    break;
            }
        }
        return $next($request);
    }

    /**
     * @param $currentUser
     * @param $moduleName
     * @return bool
     */
    public static function role($currentUser, $moduleName)
    {
        $roles = User::role;
        if (count($moduleName) != 5) {
            return false;
        }
        list(, , $catalogue, $control, $acion) = $moduleName;
        if ($currentUser['role'] == 'user') {
            if (empty($roles[$currentUser['role']][$catalogue][$control][$acion])) {
                return false;
            }
        } elseif ($currentUser['role'] == 'manager') {
            $common = self::my_merge($roles['user'], $roles[$currentUser['role']]['common']);
            if (empty($common[$catalogue][$control][$acion])) {
                $role = empty($roles[$currentUser['role']][$catalogue][$control]) ? [] :
                    $roles[$currentUser['role']][$catalogue][$control];
                if ($control == 'process') {
                    return true;
                }
                if (empty($role) || !in_array($role['role'], $currentUser['privileges'])) {
                    return false;
                }
            }
        } elseif ($currentUser['role'] != 'root') {
            return false;
        }
        return true;
    }

    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    public static function my_merge(&$a, $b)
    {

        foreach ($a as $key => &$val) {
            if (is_array($val) && array_key_exists($key, $b) && is_array($b[$key])) {
                self::my_merge($val, $b[$key]);
                $val += $b[$key];
            } elseif (is_array($val) || (array_key_exists($key, $b) && is_array($b[$key]))) {
                $val = is_array($val) ? $val : $b[$key];
            }
        }
        return $a;
    }
}