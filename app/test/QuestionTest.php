<?php

declare(strict_types=1);

namespace app\test;


use app\common\HttpTest;
use app\control\controller\exam\Question;
use app\Request;

/**
 * Class QuestionTest
 * @package app\control\controller\exam
 */
class QuestionTest extends HttpTest
{

    public function testJudgeAdd()
    {
        $model = new Question();
        $request = new Request();
        $request->withPost([
            'name' => '测试判断题',
            'type' => 'judge',
            'score' => '2',
            'difficultyLevel' => 'nan',
            'knowledge' => '2',
            'subject' => '3',
            'analysis' => '判断题解析',
            'answer' => true,
            'options' => '',
        ]);
        $json = $model->add($request);
        $response = $this->request($json);
        var_dump($response);
        static::assertContains('ok', $response, '添加失败');
    }

    public function testChoiceAdd()
    {
        $model = new Question();
        $request = new Request();
        $request->withPost([
            'name' => '选择圆周率',
            'type' => 'choice',
            'score' => '2',
            'difficultyLevel' => 'nan',
            'knowledge' => '2',
            'subject' => '3',
            'analysis' => '单选题解析',
            'answer' => 'A',
            'options' => ['3.14','6.28','9.00'],
        ]);
        $json = $model->add($request);
        $response = $this->request($json);
        var_dump($response);
        static::assertContains('ok', $response, '添加失败');
    }

    public function testChoicesAdd()
    {
        $model = new Question();
        $request = new Request();
        $request->withPost([
            'name' => '选择数字',
            'type' => 'choices',
            'score' => '2',
            'difficultyLevel' => 'nan',
            'knowledge' => '2',
            'subject' => '3',
            'analysis' => '单选题解析',
            'answer' => ['A','C'],
            'options' => ['3.14','v','9.00','a'],
        ]);
        $json = $model->add($request);
        $response = $this->request($json);
        var_dump($response);
        static::assertContains('ok', $response, '添加失败');
    }

    public function testCompositionAdd()
    {
        $model = new Question();
        $request = new Request();
        $request->withPost([
            'name' => '测试作文题',
            'type' => 'composition',
            'score' => '30',
            'difficultyLevel' => 'nan',
            'knowledge' => '2',
            'subject' => '3',
            'analysis' => '单选题解析',
            'answer' => '从前有座山,山里有座庙',
            'options' => [],
        ]);
        $json = $model->add($request);
        $response = $this->request($json);
        var_dump($response);
        static::assertContains('ok', $response, '添加失败');
    }

    public function testTypingAdd()
    {
        $model = new Question();
        $request = new Request();
        $request->withPost([
            'name' => '测试打字题',
            'type' => 'typing',
            'score' => '10',
            'difficultyLevel' => 'nan',
            'knowledge' => '2',
            'subject' => '3',
            'analysis' => '单选题解析',
            'answer' => ['time' => '5', 'number' => '20'],
            'options' => [],
        ]);
        $json = $model->add($request);
        $response = $this->request($json);
        var_dump($response);
        static::assertContains('ok', $response, '添加失败');
    }

    public function testHandelAdd()
    {
        $model = new Question();
        $request = new Request();
        $request->withPost([
            'name' => '测试操作题',
            'type' => 'handel',
            'score' => '20',
            'difficultyLevel' => 'nan',
            'knowledge' => '2',
            'subject' => '3',
            'analysis' => '测试操作题解析',
            'answer' => ['url' => 'http://www.baidu.com/img/flexible/logo/pc/result.png'],
            'options' => [],
        ]);
        $json = $model->add($request);
        $response = $this->request($json);
        var_dump($response);
        static::assertContains('ok', $response, '添加失败');
    }

    public function testQuestionModify()
    {
        $model = new Question();
        $request = new Request();
        $request->withPost([
            'id' => 5,
            'name' => '测试打字 修改',
            'type' => 'typing',
            'score' => '10',
            'difficultyLevel' => 'nan',
            'knowledge' => '2',
            'subject' => '3',
            'analysis' => '测试打字 修改',
            'answer' => ['time' => '5', 'number' => '20'],
            'options' => [],
        ]);
        $json = $model->modify($request);
        $response = $this->request($json);
        var_dump($response);
        static::assertContains('ok', $response, '添加失败');
    }

    /**
     * 判断试题答案
     */
    public function testQuestionVerify()
    {
        $questionId = 1654;
        $content = 0;
        $result = \app\common\model\exam\Question::verify($questionId,$content);
        var_dump($result);
    }

    public function testa()
    {
        $time = time();
        $app = 'FVDXl1SbiXlzA3l3Vx';
        $sec = 'd6e83ddf2f87509064d47cea249e68a3';
        var_dump($time);
        var_dump(sha1($app.$sec.$time));
    }
}
