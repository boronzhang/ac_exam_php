<?php

namespace app\h5\controller;

use think\facade\App;

class Error
{
    /**
     * 默认调用方法
     * @param $method
     * @param $args
     * @noinspection PhpIncludeInspection
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function __call($method, $args)
    {
        $tplFile = App::getAppPath() . 'view/index/home.html';
        $assign = [
            'staticPath' => '/static/h5'
        ];
        include $tplFile;
    }
}