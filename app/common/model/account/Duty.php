<?php

declare(strict_types=1);

namespace app\common\model\account;

use Exception;
use mb\helper\Collection;
use think\facade\Db;
use think\facade\Log;
use think\Model;

/**
 * Class Duty
 * @package app\common\model\account
 */
class Duty
{
    /**
     * filter.title
     * @param array $filters
     * @param int $pIndex
     * @param int $pSize
     * @param int $total
     * @return array
     */
    public static function search(array $filters, int $pIndex = 1, int $pSize = 10, &$total = 0)
    {
        $where = [];
        if (!empty($filters['title'])) {
            $where[] = ['title', 'like', "%{$filters['title']}%"];
        }
        try {
            $total = Db::table('account_dutys')
                ->where($where)
                ->count();

            $query = Db::table('account_dutys')
                ->where($where);
            if (!empty($pIndex)) {
                $query->page($pIndex, $pSize);
            }
            if (!empty($filters['order'])) {
                $query->order($filters['order']);
            }
            $dataSet = $query->select()->toArray();
            if (!empty($dataSet)) {
                return array_map(function ($row) {
                    return Collection::keyStyle($row, Collection::NAME_STYLE_JAVA);
                }, $dataSet);
            }
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return [];
    }

    /**
     * dutyInfo.title
     * @param $dutyInfo
     * @return int|string
     */
    public static function add($dutyInfo)
    {
        $newRow = Collection::elements(['title'], $dutyInfo);
        try {
            return Db::table('account_dutys')
                ->insertGetId($newRow);
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }


    /**
     * @param $filters
     * @return array|bool|Model|null
     */
    public static function fetch($filters)
    {
        $where = self::parseFilters($filters);
        try {
            return Db::table('account_dutys')
                ->where($where)
                ->find();
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }

    /**
     * newState.title
     * @param $filters
     * @param $newState
     * @return bool
     */
    public static function update($filters,$newState)
    {
        $where = self::parseFilters($filters);
        try {
            $offect = Db::table('account_dutys')
                ->where($where)
                ->update($newState);
            if ($offect === 1) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }

    /**
     * @param $filters
     * @return bool
     */
    public static function remove($filters)
    {
        $where = self::parseFilters($filters);
        try {
            $offect = Db::table('account_dutys')
                ->where($where)
                ->delete();
            if ($offect === 1) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }

    /**
     * dutyFilters.id
     * dutyFilters.ids
     * dutyFilters (int) 为ID
     * @param $dutyFilters
     * @return array
     */
    public static function parseFilters($dutyFilters)
    {
        $filters = [];
        if (is_array($dutyFilters)) {
            if (!empty($dutyFilters['id'])) {
                $filters[] = ['id','=',$dutyFilters['id']];
            }
            if (!empty($dutyFilters['ids'])) {
                $filters[] = ['id','in',$dutyFilters['ids']];
            }
        } else {
            $filters[] = ['id','=',intval($dutyFilters)];
        }
        return $filters;
    }
}