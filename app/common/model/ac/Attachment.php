<?php

declare(strict_types=1);

namespace app\common\model\ac;

use app\common\model\Base;
use think\facade\Db;
use Exception;
use Error;
use think\facade\Log;

/**
 * Class Attachment
 * @package app\common\model\ac
 */
class Attachment
{
    /**
     * @param int $pIndex
     * @param int $pSize
     * @param int $total
     * @return array
     */
    /*public static function search(int $pIndex = 1, int $pSize = 10, &$total = 0)
    {
        try {
            $total = Db::table('ac_attachments')->count();
            $query = Db::table('ac_attachments');
            if (!empty($pIndex)) {
                $query->page($pIndex, $pSize);
            }
            $data = $query->select()->toArray();
            if (!empty($data)) {
                $storage = Base::createStorage();
                $data = array_map(function ($val) use ($storage) {
                    $val['resource'] = $storage->domainUrl() . $val['resource'];
                }, $data);
                return $data;
            } else {
                return [];
            }
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return [];
    }*/

    /**
     * @param string $tmp
     * @param string $suffix
     * @return array|Error|\think\Exception
     */
    /*public static function add(string $tmp, string $suffix)
    {
        try {
            $storage = Base::createStorage();
            $video = "exam/attach/" . md5(uniqid()) . $suffix;
            $tmp = $storage->put($video, $tmp);
            $data['resource'] = $tmp['filename'];
            if (is_error($tmp)) {
                return error(-1, '上传失败请重试');
            }
            $data['title'] = md5(uniqid());
            $data['time_created'] = time();
            $id = Db::table('ac_attachments')->insertGetId($data);
            return ['id' => $id,'url' => $tmp['url']];
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return error(-1, '上传失败请重试');
    }*/

    /**
     * @param int $id
     * @return bool
     */
    /*public static function delete(int $id)
    {
        try {
            $resouce = Db::table('ac_attachments')->where(['id' => $id])->value('resource');
            $res = Db::table('ac_attachments')->where(['id' => $id])->delete();
            $res = $res !== false;
            if (!$res) {
                return false;
            }
            $storage = Base::createStorage();
            $resouce = $storage->domainUrl() . $resouce;
            $storage->delete($resouce, true);
            return true;
        } catch (Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }*/
}