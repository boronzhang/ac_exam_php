<?php

declare(strict_types=1);

namespace app\dock\controller;

use app\BaseController;
use app\common\model\account\User as UserModel;
use app\common\model\exam\paper\Record;
use app\control\model\User;
use mb\helper\Collection;
use think\Exception;
use think\Request;
use think\response\Json;
use app\common\model\exam\Paper;

/**
 * Class Personal
 * @package app\dock\controller
 */
class Personal extends BaseController
{
    /**
     * @param Request $request
     * @return Json
     */
    public function search(Request $request)
    {
        $input = $request->post();
        if (empty($input['paperId'])) {
            return payload(error(-10, '参数不完整'));
        }
        $user = User::fetchCurrent();
        $filters = [];
        $filters['status'] = 'end';
        $filters['userId'] = $user['id'];
        $filters['paperId'] = $input['paperId'];
        $dataSet = Record::search($filters, 0);
        $dataSet = array_map(
            function ($rec) {
                $rec['pass'] = ($rec['score'] > $rec['throughPoints']) ? true : false;
                if ($rec['markId']) {
                    $userInfo = UserModel::fetch($rec['markId']);
                    $rec['markTitle'] = isset($userInfo['uid']) ? $userInfo['uid'] : '';
                } else {
                    $rec['markTitle'] = '';
                }
                $rec = Collection::elements(
                    [
                        'id',
                        'paperId',
                        'userId',
                        'status',
                        'pass',
                        'score',
                        'totalPoints',
                        'throughPoints',
                        'timeEnd',
                        'title',
                        'markTitle',
                        'markStatus'
                    ],
                    $rec
                );
                return $rec;
            },
            $dataSet
        );
        return payload(['dataSet' => $dataSet]);
    }
}