<?php

declare(strict_types=1);

namespace app\control\controller\ac;

use app\BaseController;
use app\common\model\ac\Attachment as ModelAttachment;
use app\Request;
use think\facade\App;
use think\response\Json;

/**
 * Class Attachment
 * @package app\control\controller\ac
 */
class Attachment extends BaseController
{
    /**
     * @param Request $request
     * @return Json
     * @api {post}  /ac/attachment/search 附件列表
     * @apiName           sort1
     * @apiGroup          Attach
     * @apiDescription    附件列表
     *
     * @apiParam {Number} [pageIndex=1] 当前页码, 默认第1页
     * @apiParam {Number} [pageSize=30] 分页大小, 默认30
     *
     * @apiSuccess {Object[]} dataSet 附件列表
     * @apiSuccess {Number} dataSet.id 附件编码
     * @apiSuccess {String} dataSet.resource 附件路径
     * @apiSuccess {Number} dtaSet.time_created 附件上传时间
     * @apiSuccess {Number} total 附件总数
     *
     * @apiSuccessExample 成功示例
     * {
     *     dataSet: [
     *          {
     *              id: 2,
     *              resource: "http://res.huihujiankang.cn/attch/image/44e9af38a3af7fe46bee38f98ff4faf2.jpg",
     *              time_created: 1569292474
     *          },
     *      ],
     *     total: 2,
     *     errCode: 0,
     *     errMsg: "ok"
     * }
     *
     */
    public function search(Request $request)
    {
        $input = $request->post();
        $pageIndex = empty($input['current']) ? 1 : intval($input['current']);
        $pageSize = empty($input['pageSize']) ? 10 : intval($input['pageSize']);
        $total = 0;
        $dataSet = ModelAttachment::search($pageIndex, $pageSize, $total);
        return payload(['dataSet' => $dataSet, 'total' => $total]);
    }

    /**
     * @param Request $request
     * @return Json
     * @api {post}  /ac/attachment/add 附件上传
     * @apiName           sort2
     * @apiGroup          Attach
     * @apiDescription    附件上传
     *
     * @apiParam {File} file 文件
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {String} url 返回url
     *
     * @apiSuccessExample 成功示例
     * {
     *     id: 1,
     *     url: 'https:xxxx',
     *     errCode: 0,
     *     errMsg: "ok"
     * }
     */
    public function add(Request $request)
    {
        $file = $request->file('file');
        $filePath = $file->getRealPath();
        $ext = $file->getOriginalExtension();
        if (empty($file)) {
            return payload(error(-1, '文件不能为空'));
        }
        $res = ModelAttachment::add($filePath, $ext);
        if (is_error($res)) {
            return payload(error(-1, '上传失败请重试'));
        }
        return payload($res);
    }

    /**
     * @param Request $request
     * @return Json
     * @api {post} /ac/attachment/delete 附件删除
     * @apiGroup Attach
     * @apiName sort3
     * @apiVersion 1.0.0
     *
     * @apiDescription 附件删除
     *
     * @apiParam {Number} id  附件id
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code":0,"message":""}
     *
     * @apiErrorExample {json} Error-Response:
     * {"code":5001,"message":"接口异常"}
     *
     */
    public function delete(Request $request)
    {
        $input = $request->post();
        if (empty($input['id'])) {
            return payload(error(-1, '参数不完整'));
        }
        $res = ModelAttachment::delete(intval($input['id']));
        if (!$res) {
            return payload(error(-1, '删除失败'));
        }
        return payload([]);
    }

    public function download(Request $request)
    {
        $input = $request->param();
        if (empty($input['template'])) {
            return payload(error(-10, '参数不完整'));
        }
        switch ($input['template']) {
            case 'question':
                $file = 'question.xls';
                $name = "导入试题.xls";
                break;
            case 'department':
                $file = 'department.xls';
                $name = "导入部门.xls";
                break;
            case 'knowledge':
                $file = 'knowledge.xls';
                $name = "科目知识点导入.xls";
                break;
            case 'user':
                $file = 'user.xls';
                $name = "导入用户.xls";
                break;
            default:
                $file = 'operate.docx';
                $name = "考试平台操作手册.docx";
                break;
        }
        return download(App::getAppPath() . "assets/template/{$file}", $name);
    }
}