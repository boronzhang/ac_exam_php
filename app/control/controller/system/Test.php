<?php

declare(strict_types=1);

namespace app\dock\controller;

use GuzzleHttp\Client;

class Test
{
    public function test()
    {
//        dump(self::get_client_ip());
        $client = new Client();
        $ip = self::get_client_ip();
        $response = $client->post("http://whois.pconline.com.cn/ipJson.jsp?ip={$ip}&json=true");
        $content = $response->getBody()->getContents();
        $content = iconv('GBK', 'utf-8', $content);
        $content = str_replace(["\r\n", "\r", "\n"], "", $content);
        $res = json_decode($content, true, 512, JSON_BIGINT_AS_STRING);
        if ($res['pro'] == '山西省') {
            header("Location: https://www.baidu.com/");
        } else {
            header("Location: https://www.taobao.com/");
        }
//        dump($res['pro']);
//        dump($res['city']);
    }

    public static function get_client_ip($type = 0)
    {
        $type = $type ? 1 : 0;
        static $ip = null;
        if ($ip !== null) {
            return $ip[$type];
        }
        if (isset($_SERVER['HTTP_X_REAL_IP'])) {//nginx 代理模式下，获取客户端真实IP
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {//客户端的ip
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {//浏览当前页面的用户计算机的网关
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) {
                unset($arr[$pos]);
            }
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];//浏览当前页面的用户计算机的ip地址
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u", ip2long($ip));
        $ip = $long ? [$ip, $long] : ['0.0.0.0', 0];
        return $ip[$type];
    }
}