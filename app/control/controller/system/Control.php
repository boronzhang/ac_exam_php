<?php

declare(strict_types=1);

namespace app\control\controller\system;

use app\common\model\Base;
use app\control\model\System;
use app\Request;
use think\facade\Cache;

/**
 * Class Control
 * @package app\control\controller\system
 */
class Control
{
    /**
     * @param Request $request
     * @return array|\Error|\think\response\Json
     */
    public function set(Request $request)
    {
        $input = $request->post();
//        $params = Base::existsParams($input, ['title', 'scoreMenu']);
        $params = Base::existsParams($input, ['title']);
        if (is_error($params)) {
            return $params;
        }
        $result = System::set('title', $params['title']);
        if (!$result) {
            return payload(error(-22, '标题修改失败'));
        }
//        $res = System::set('scoreMenu', $params['scoreMenu']);
//        if (!$res) {·
//            return payload(error(-22, '成绩查询菜单修改失败'));
//        }
        Cache::set('controlTitle', $params['title']);
        return payload();
    }

    /**
     * @return \think\response\Json
     */
    public function get()
    {
        $title = System::get('title');
//        $scoreMenu = System::get('scoreMenu');
//        if (empty($scoreMenu) || ($scoreMenu == 'true')) {
//            $scoreMenu = true;
//        } else {
//            $scoreMenu = false;
//        }
//        return payload(['dataSet' => ['title' => $title, 'scoreMenu' => $scoreMenu]]);
        return payload(['dataSet' => ['title' => $title]]);
    }

}