<?php

declare(strict_types=1);

namespace app\control\controller\book;

use app\BaseController;
use app\Request;
use app\common\model\book\Volume as ModelVolume;
use app\common\model\book\Chapter;
use think\response\Json;

/**
 * Class Volume
 * @package app\control\controller\book
 */
class Volume extends BaseController
{
    /**
     * @param Request $request
     * @return Json
     * @api {post} /book/volume/search 电子书章列表
     * @apiGroup Book
     * @apiName sort2
     * @apiVersion 1.0.0
     *
     * @apiDescription 电子书章列表
     *
     * @apiParam {Number} [current]  页码
     * @apiParam {Number} [pageSize]  页数
     * @apiParam {String} [title]  科目名称
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code":0,"message":"","dataSet":[], "total" : 0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"code":5001,"message":"接口异常"}
     */
    public function search(Request $request)
    {
        $input = $request->post();
        $pageIndex = empty($input['current']) ? 1 : intval($input['current']);
        $pageSize = empty($input['pageSize']) ? 10 : intval($input['pageSize']);
        $total = 0;
        $filters = [];
        if (!empty($input['title'])) {
            $filters['title'] = $input['title'];
        }
        $dataSet = ModelVolume::search($filters, $pageIndex, $pageSize, $total);
        return payload(['dataSet' => $dataSet, 'total' => $total]);
    }

    /**
     * @param Request $request
     * @return Json
     * @api {post} /book/volume/modify 电子书章新增/编辑
     * @apiGroup Book
     * @apiName sort3
     * @apiVersion 1.0.0
     *
     * @apiDescription 电子书章新增/编辑
     *
     * @apiParam {Number} [id]  章id 新增传0
     * @apiParam {Number} subjectId 科目id
     * @apiParam {String} title  章名称
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Number} id   章id
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code":0,"message":"", "id" : 1}
     *
     * @apiErrorExample {json} Error-Response:
     * {"code":5001,"message":"接口异常"}
     */
    public function modify(Request $request)
    {
        $input = $request->post();
        if (empty($input['subjectId']) || empty($input['title'])) {
            return payload(error(-1, '参数不完整'));
        }
        $id = empty($input['id']) ? 0 : intval($input['id']);
        $id = ModelVolume::modify($input, $id);
        return payload(['id' => $id]);
    }

    /**
     * @param Request $request
     * @return array|Json
     * @api {post} /book/volume/delete 电子书章删除
     * @apiGroup Book
     * @apiName sort4
     * @apiVersion 1.0.0
     *
     * @apiDescription 电子书章删除
     *
     * @apiParam {Number} id  章id
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code":0,"message":""}
     *
     * @apiErrorExample {json} Error-Response:
     * {"code":5001,"message":"接口异常"}
     */
    public function delete(Request $request)
    {
        $input = $request->post();
        if (empty($input['id'])) {
            return payload(error(-1, '参数不完整'));
        }
        $chapter = Chapter::search(['volume_id' => intval($input['id'])], 0);
        if (!empty($chapter)) {
            return payload(error(-1, '该章下有小节，请先删除小节'));
        }
        $res = ModelVolume::delete(['id' => intval($input['id'])]);
        if (!$res) {
            return error(-1, '操作失败');
        }
        return payload([]);
    }
}