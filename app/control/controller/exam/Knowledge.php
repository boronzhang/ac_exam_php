<?php

declare(strict_types=1);

namespace app\control\controller\exam;

use app\BaseController;
use app\common\model\exam\Knowledge as ModelKnowledge;
use app\common\model\exam\Subject;
use app\Request;
use think\response\Json;

/**
 * Class Knowledge
 * @package app\control\controller\exam
 */
class Knowledge extends BaseController
{
    /**
     * @param Request $request
     * @return Json
     * @api {post} /exam/knowledge/search 知识点列表
     * @apiGroup Exam-Subject
     * @apiName sort6
     * @apiVersion 1.0.0
     *
     * @apiDescription 知识点列表
     *
     * @apiParam {Number} [current]  页码
     * @apiParam {Number} [pageSize]  页数
     * @apiParam {Number} subjectId  科目id
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     * @apiSuccess {Number} total    总数
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code":0,"message":"","dataSet":[], "total" : 0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"code":5001,"message":"接口异常"}
     */
    public function search(Request $request)
    {
        $input = $request->post();
        $pageIndex = empty($input['current']) ? 1 : intval($input['current']);
        $pageSize = empty($input['pageSize']) ? 10 : intval($input['pageSize']);
        $total = 0;
        $filters = [];
        if (empty($input['subjectId'])) {
            return payload(error(-1, '参数不完整'));
        } else {
            $filters['subjectId'] = $input['subjectId'];
        }
        if (!empty($input['title'])) {
            $filters['title'] = $input['title'];
        }
        $dataSet = ModelKnowledge::search($filters, $pageIndex, $pageSize, $total);
        $dataSet = array_map(function ($val) {
            $subject = Subject::fetch($val['subjectId']);
            $val['subjectTitle'] = $subject['title'];
            return $val;
        }, $dataSet);
        return payload(['dataSet' => $dataSet, 'total' => $total]);
    }

    /**
     * @param Request $request
     * @return Json
     * @api {post} /exam/knowledge/modify 知识点新增/修改
     * @apiGroup Exam-Subject
     * @apiName sort7
     * @apiVersion 1.0.0
     *
     * @apiDescription 知识点新增/修改
     *
     * @apiParam {Number} [id]  知识点id
     * @apiParam {Number} subjectId  科目id
     * @apiParam {String} title 知识点标题
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Number} id    知识点id
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code":0,"message":"","id":1}
     *
     * @apiErrorExample {json} Error-Response:
     * {"code":5001,"message":"接口异常"}
     */
    public function modify(Request $request)
    {
        $input = $request->post();
        if (empty($input['subjectId']) || empty($input['title'])) {
            return payload(error(-1, '参数不完整'));
        }
        $id = empty($input['id']) ? 0 : intval($input['id']);
        $data = ['subjectId' => $input['subjectId'], 'title' => $input['title']];
        $check = ModelKnowledge::check($input['title'], $id);
        if (!$check) {
            return payload(error(-1, '知识点名已存在'));
        }
        if (empty($id)) {
            $id = ModelKnowledge::add($data);
        } else {
            $res = ModelKnowledge::update(['id' => $id], $data);
            if (!$res) {
                return payload(error(-1, '操作失败'));
            }
        }
        return payload(['id' => $id]);
    }

    /**
     * @param Request $request
     * @return Json
     * @api {post} /exam/knowledge/delete 知识点删除
     * @apiGroup Exam-Subject
     * @apiName sort8
     * @apiVersion 1.0.0
     *
     * @apiDescription 知识点删除
     *
     * @apiParam {Number} id  知识点id
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     *
     * @apiSuccessExample {json} Success-Response:
     * {"code":0,"message":""}
     *
     * @apiErrorExample {json} Error-Response:
     * {"code":5001,"message":"接口异常"}
     */
    public function delete(Request $request)
    {
        $input = $request->post();
        if (empty($input['ids']) || empty($input['type'])) {
            return payload(error(-1, '参数不完整'));
        }
        if ($input['type'] == 'batch') {
            $ids = implode(',', $input['ids']);
        } else {
            $ids = (string)$input['ids'];
        }
        $res = ModelKnowledge::delete($input['type'], $ids);
        if (!$res) {
            return payload(error(-1, '操作失败'));
        }
        return payload([]);
    }

}