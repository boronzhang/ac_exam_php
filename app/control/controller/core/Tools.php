<?php

declare(strict_types=1);

namespace app\control\controller\core;

use app\common\model\Base;
use app\Request;
use think\response\Json;

/**
 * Class Tools
 * @package app\control\controller\core
 */
class Tools
{
    /**
     * @param Request $request
     * @return Json
     *
     * @api {post} /core/tools/upload 上传文件
     * @apiGroup Tools
     * @apiName sort1
     * @apiVersion 1.0.0
     *
     * @apiDescription 上传文件
     *
     * @apiParam {number} type  file - 文件 image - 图片 可批量上传
     * @apiParam {file} file  文件
     *
     * @apiSuccess {Number} code    状态码，0：请求成功
     * @apiSuccess {String} message   提示信息
     * @apiSuccess {Object} dataSet    返回数据
     *
     * @apiSuccessExample {json} Success-Response:
     * {"errCode":0,"errMsg":"","dataSet":[],"total":0}
     *
     * @apiErrorExample {json} Error-Response:
     * {"errCode":5001,"errMsg":"接口异常"}
     *
     */
    public function upload(Request $request)
    {
        $input = $request->param();
        $params = Base::existsParams($input, ['type']);
        if (is_error($params)) {
            return payload($params);
        }
        $res = Base::uploadfile($params['type']);
        if (empty($res)) {
            return payload(error(-20, '上传失败'));
        }
        return payload(['dataSet' => $res]);
    }
}