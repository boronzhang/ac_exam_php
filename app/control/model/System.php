<?php


namespace app\control\model;

use think\facade\Cache;
use think\facade\Db;
use think\facade\Log;

/**
 * Class System
 * @package app\control\model
 */
class System
{
    /**
     * @param $key
     * @return mixed|string
     */
    public static function get($key)
    {
        if ($key == 'title') {
            $value = Cache::get('controlTitle');
            if (!empty($value)) {
                return $value;
            }
        }
        $value = Db::table('ac_config')
            ->where('key', '=', $key)
            ->value('value');
        if (empty($value) && ($key == 'title')) {
            $value = '在线考试平台';
            Cache::set('controlTitle', $value);
        }
        return $value;
    }

    /**
     * @param $key
     * @param $value
     * @return array|bool|\Error|\think\Exception
     */
    public static function set($key, $value)
    {
        try {
            $exists = Db::table('ac_config')
                ->where('key', '=', $key)
                ->value('value');
            if ($value === $exists) {
                return error(-25, '与原先参数一致,无需修改');
            }
            if (empty($exists)) {
                Db::table('ac_config')->insert(['key' => $key, 'value' => $value]);
            } else {
                Db::table('ac_config')
                    ->where('key', '=', $key)
                    ->update(['value' => $value]);
            }
            return true;
        } catch (\Exception $e) {
            Log::channel('myError')->write($e->getMessage(), \think\Log::ERROR);
        }
        return false;
    }
}