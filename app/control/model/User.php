<?php

declare(strict_types=1);

namespace app\control\model;

use app\common\model\account\User as UserModel;

/**
 * Class User
 * @package app\control\controller\model
 */
class User
{
    public const role = [
        'user' => [
            'system' => [
                'account' => [
                    'current' => true
                ],
                'control' => [
                    'get' => true
                ],
                'news' => [
                    'founder' => true,
                    'search' => true,
                    'detail' => true,
                    'addView' => true,
                ],
                'book' => [
                    'tree' => true,
                ]
            ],
            'book' => [
                'chapter' => [
                    'search' => true
                ]
            ],
            'exam' => [
                'subject' => [
                    'search' => true
                ],
                'knowledge' => [
                    'search' => true
                ],
                'exam' => [
                    'detail' => true,
                    'search' => true,
                    'create' => true,
                    'save' => true,
                ],
                'personal' => [
                    'search' => true,
                    'statistics' => true,
                    'ranking' => true,
                    'preview' => true,
                    'statisticsDetail' => true,
                ]
            ],
            'account' => [
                'user' => [
                    'modify' => true,
                    'loginOut' => true,
                    'updatePassword' => true,
                ],
            ],
            'core' => [
                'tools' => [
                    'upload' => true,
                ]
            ]

        ],
        'manager' => [
            'system' => [
                'news' => [
                    'role' => 'newsManger'
                ],
                'book' => [
                    'role' => 'examManger'
                ]
            ],
            'account' => [
                'user' => [
                    'role' => 'AccountManger'
                ],
            ],
            'book' => [
                'chapter' => [
                    'role' => 'examManger'
                ],
                'volume' => [
                    'role' => 'examManger'
                ]
            ],
            'exam' => [
                'question' => [
                    'role' => 'examManger'
                ],
                'paper' => [
                    'role' => 'testPaperManger'
                ],
                'subject' => [
                    'role' => 'examManger'
                ],
                'process' => [
                    'role' => 'courseManger'
                ]
            ],
            'common' => [
                'system' => [
                    'account' => [
                        'current' => true
                    ]
                ],
                'account' => [
                    'duty' => [
                        'search' => true
                    ],
                    'department' => [
                        'search' => true
                    ],
                    'user' => [
                        'modify' => true,
                        'search' => true
                    ]
                ],
            ]
        ],
    ];

    /**
     * 获取当前用户信息
     * @return array
     */
    public static function fetchCurrent()
    {
        static $currentUser;
        if (empty($currentUser)) {
            $currentUser = session('currentUser');
            if (empty($currentUser)) {
                abort(401, '登录未授权');
            }
            $currentUser = unserialize($currentUser);
        }
        return $currentUser;
    }
}