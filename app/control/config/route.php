<?php

declare(strict_types=1);

use think\facade\Route;

Route::get(':main/:sub/:action', ':main.:sub/:action');
Route::post(':main/:sub/:action', ':main.:sub/:action');
