<?php

declare(strict_types=1);

namespace app\wander\controller;

use app\control\model\System;
use think\facade\App;

/**
 * 默认控制器
 * @package app\wander\controller
 */
class Error
{
    /**
     * 默认调用方法
     * @param $method
     * @param $args
     * @noinspection PhpIncludeInspection
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function __call($method, $args)
    {
        $tplFile = App::getAppPath() . 'view/loader.html';
        $title = System::get('title');
        $assign = [
            'staticPath' => '/static/control'
        ];
        include $tplFile;
    }
}
