## 介绍
 运行环境要求PHP7.1+ mysql5.7+
* 采用`PHP7`强类型（严格模式）
* 题库管理 支持多种试题类型和录题方式
* 考生管理 快速导入考生信息分类管理
* 答题模式 提供四种考试模式可自定义 提供考试列表整卷或逐题展示 支持考试结束查看成绩以及错题
* 组卷方式 支持随机组卷和人工组卷 参数设置 满足不同场景需求，按不同知识点/难易 程度抽题
* 防作弊 答题时切屏强制交卷 防丢失 答题时意外关机，可自动保存答题结果

## 安装

项目目录中有安装文档，请阅读安装文档  
安装文档中采用的是phpstudy，也可使用其他集成软件  
windows安装视频：https://www.bilibili.com/video/BV1HU4y1c7ih/  
Linux版本的安装教程正在更新中

## 学习交流

使用过程中的问题或者是bug反馈可以联系艾创服务号QQ：1779257099或扫下方二维码添加服务号微信（备注来自码云）

## 合作

如果您是机构，企业，政府，需要单独搭建考试平台的，欢迎咨询，可以提供部署和定制开发服务。  
合作联系：  
![联系QQ:1779257099](https://demo.exam.aichuang.team/img/%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.png)

## 在线考试系统部分功能截图

* 题库管理
![题库管理](https://demo.exam.aichuang.team/img/%E9%A2%98%E5%BA%93%E7%AE%A1%E7%90%86.png)
* 组卷
![组卷](https://demo.exam.aichuang.team/img/%E7%BB%84%E5%8D%B7.png)
* 答题
![答题](https://demo.exam.aichuang.team/img/%E7%AD%94%E9%A2%98.png)
* 答卷记录
![答卷记录](https://demo.exam.aichuang.team/img/%E7%AD%94%E5%8D%B7%E8%AE%B0%E5%BD%95.png)
* 查看答卷及错题
![查看答卷及错题](https://demo.exam.aichuang.team/img/%E6%9F%A5%E7%9C%8B%E7%AD%94%E5%8D%B7%E5%8F%8A%E9%94%99%E9%A2%98.png)

## 版权信息

艾创在线考试系统1.0开源版本仅供个人学习交流 禁商用  